import 'dart:io';

void main(List<String> args) {
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';
  print(word+second+third+fourth+fifth+sixth+seventh);

  var sentence = 'I am going to be Flutter Developer';
  var firstWord = sentence[0];
  var secondWord = sentence[2] + sentence[3];
  var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
  var fourthWord = sentence[11] + sentence[12];
  var fifthWord = sentence[14] + sentence[15];
  var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22] + sentence[23];
  var seventhWord = sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33];

  print('First Word: ' + firstWord);
  print('Second Word: ' + secondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
  print('Seventh Word: ' + seventhWord);

  print("Nama depan: ");
  String? firstName  = stdin.readLineSync();
  print("Nama belakang: ");
  String? lastName = stdin.readLineSync();
  
  print("Nama lengkap anda adalah ${firstName} ${lastName}");

  var a = 5;
  var b = 10;
  var kali = a*b;
  var tambah = a+b;
  var kurang = a-b;
  var bagi = a/b;
  print('Penjumlahan: ${tambah}');
  print('Pengurangan: ${kurang}');
  print('Perkallian: ${kali}');
  print('Pembagian: ${bagi}');

  print("Apakah Anda ingin menginstall aplikasi? (y/n) ");
  String? answer  = stdin.readLineSync();
  if (answer == 'y') {
    print('anda akan menginstall aplikasi dart');
  } else {
    print('aborted');
  }

  // Output untuk Input nama = '' dan peran = '' " , apabila kosong semua Nama harus diisi!"
 
//Output untuk Input nama = 'John' dan peran = '' "apabila cuman diisi nama akan muncul Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir' "Selamat datang di Dunia Werewolf, Jane" "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard' "Selamat datang di Dunia Werewolf, Jenita" "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf' "Selamat datang di Dunia Werewolf, Junaedi" "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"

  print("nama Anda: (John/Jane/Jenita/Junaedi)");
  String? nama = stdin.readLineSync();
  print("peran Anda: (Penyihir/Guard/Werewolf)");
  String? peran = stdin.readLineSync();
  if (nama == '') {
    print("Nama harus diisi!");
  } else if(nama=='John' && peran == ''){
    print("Pilih peranmu untuk memulai game!");
  }else if(nama=='Jane' && peran == 'Penyihir'){
    print("Selamat datang di Dunia Werewolf, Jane" "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
  }else if(nama == 'Jenita' && peran == 'Guard'){
    print("Selamat datang di Dunia Werewolf, Jenita" "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  }else if(nama == 'Junaedi' && peran == 'Werewolf'){
    print("Selamat datang di Dunia Werewolf, Junaedi" "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
  }

  print("Masukkan nama hari (penulisan diawali huruf kapital)");
  String? hari  = stdin.readLineSync();
  switch (hari) {
    case "Senin ":
      print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
      break;
    case "Selasa":
      print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
      break;
    case "Rabu":
      print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri");
      break;
    case "Kamis":
      print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain");
      break;
    case "Jumat":
     print("Hidup tak selamanya tentang pacar.");
      break;
    case "Sabtu":
      print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan");
      break;
    case "Minggu":
      print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani");
      break;
    default:
      print("Hari yang anda masukkan tidak valid atau terdapat kesalahan penulisan");
  } 

  var tanggal = 21;
  var bulan = 1;
  var tahun = 1945;
  var namaBulan;

  switch (bulan) {
    case 1:
      namaBulan = 'Januari';
      break;
    case 2:
      namaBulan = 'Februari';
      break;
    case 3:
      namaBulan = 'Maret';
      break;
    case 4:
      namaBulan = 'April';
      break;
    case 5:
      namaBulan = 'Mei';
      break;
    case 6:
      namaBulan = 'Juni';
      break;
    case 7:
      namaBulan = 'Juli';
      break;
    case 8:
      namaBulan = 'Agustus';
      break;
    case 9:
      namaBulan = 'September';
      break;
    case 10:
      namaBulan = 'Oktober';
      break;
    case 11:
      namaBulan = 'November';
      break;
    case 12:
      namaBulan = 'Desember';
      break;
    default:
      print("Bulan invalid");
  }

  print('$tanggal $namaBulan $tahun');
}