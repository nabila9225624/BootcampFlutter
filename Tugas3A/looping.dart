void main(List<String> args) {
  var num=1;
  while (num<=20){
    if(num%2==0){
      print("$num - I love coding");
    }
    num++;
  }

  for(var a=1; a<=20; a++){
    if (a%2 == 0) {
      print("$a - Berkualitas");
    } else if (a%2 != 0 && a%3 == 0) {
      print("$a - I love Coding");
    } else{
      print("$a - Santai");
    }
  }

  for (var i = 0; i < 4; i++) {
    String row = '';
    for (var j = 0; j < 8; j++) {
      row += '#';
    }
    print(row);
  }

  for(var b = 1; b<7; b++){
    String row = '';
    for (var c = 1; c <= b ; c++) {
      row += '#';
    }
    print(row);
  }
}