void main(List<String> args) {
  BukuAnggota buku1 = BukuAnggota("Pemrograman Dart", "John", "32343");
  buku1.tampilInfo();
}

class BukuAnggota {
  String judul;
  String pengarang;
  String isbn;

  BukuAnggota(this.judul, this.pengarang, this.isbn);

  void tambahBuku(){
    
  }

  void hapusBuku(){

  }
  
  void editBuku(){

  }

  void tampilInfo(){
    print("Judul: $judul, Pengarang: $pengarang, ISBN: $isbn");
  }
}