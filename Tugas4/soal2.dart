void main(List<String> args) {
  rangeWithStep(36, 45, 3);
}

rangeWithStep(int num1, int num2, int step){
  var result = [];
  if (num1 > num2) {
    for(var i=num1; i>num2; i--){
      if(i%step == 0){
        result.add(i);
      }
    }
  }
  else if(num1 < num2){
    for(var i=num1; i<num2; i++){
      if(i%step == 0){
        result.add(i);
      }
    }
  } 
  print(result);
}