void main(List<String> args) {
  ArmorTitan armor = ArmorTitan(5);
  armor.terjang();

  AttackTitan attack = AttackTitan(6);
  attack.punch();

  BeastTitan beast = BeastTitan(9);
  beast.lempar();

  Human human = Human(3);
  human.killAllTitan();
}

class Titan {
  late double _powerPoint;

  Titan(this._powerPoint);

  void setPowerPoint(double value) => _powerPoint = value;

  double getPowerPoint() => this._powerPoint;
}

class ArmorTitan extends Titan{
  ArmorTitan(super._powerPoint);

  void terjang(){
    print("dush.. dush..");
    print("Powerpoint = $_powerPoint");
  }
}

class AttackTitan extends Titan{
  AttackTitan(super._powerPoint);

  void punch(){
    print("blam.. blam..");
    print("Powerpoint = $_powerPoint");
  }
}

class BeastTitan extends Titan{
  BeastTitan(super._powerPoint);

  void lempar() => print("Powerpoint = $_powerPoint, wush.. Wush");
}

class Human extends Titan{
  Human(super._powerPoint);

  void killAllTitan() => print("Powerpoint = $_powerPoint, Sasageyo.. Shinzo wo sasageyo");
}