void main(List<String> args) {
  Segitiga segitiga1 = Segitiga(0.5, 20.0, 30.0);  
  print(segitiga1.luasSegitiga());

  Lingkaran lingkaran1 = Lingkaran(2.0);
  print(lingkaran1.hitungLuas());
}

class Segitiga {
  double setengah;
  double alas;
  double tinggi;

  Segitiga(this.setengah, this.alas, this.tinggi);

  luasSegitiga(){
    return setengah * alas * tinggi;
  }
}

class Lingkaran {
  late double _ruas;

  Lingkaran(this._ruas);

  double get ruas => this._ruas;

  void setRuas(double value){
    if (value < 0) {
      _ruas = value * -1;
    } else {
       _ruas = value;
    }
  }

  double hitungLuas(){
    return 3.14 * ruas * ruas;
  }
}